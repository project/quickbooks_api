/**
 * Added by mlevasseur on 2/25/2016.
 *
 * @see https://github.com/IntuitDeveloper/oauth-php/blob/master/PHPOAuthSample/index.php
 */

(function ($, Drupal) {

  Drupal.behaviors.intuitOAuthConnect = {

    attach: function (context, settings) {
      var parser = document.createElement('a');
      parser.href = document.url;

      //alert(drupalSettings.quickbooks_api.quickbooks_api.oauthUrl);

      intuit.ipp.anywhere.setup({
        menuProxy: '',
        grantUrl: drupalSettings.quickbooks_api.quickbooks_api.oauthUrl,
        // outside runnable you can point directly to the oauth.php page
      });
    }
  }
})(jQuery, Drupal);
