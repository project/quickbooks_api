/**
 * Added by Andrea(s) Speck, 17 Dec 2019
 */

(function ($, Drupal) {
  window.opener.location.href = window.opener.location.href;
  window.close();
})(jQuery, Drupal);
