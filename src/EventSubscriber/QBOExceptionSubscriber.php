<?php
namespace Drupal\quickbooks_api\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use QuickBooksOnline\API\Exception\ServiceException;
use QuickBooksOnline\API\Exception\SdkException;
use QuickBooksOnline\API\Exception\IdsException;


/**
 * Catching a Quickbooks API exception
 */
class QBOExceptionSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  public function __construct(LoggerChannelFactoryInterface $logger) {
    $this->logger = $logger;
  }

  public function onException(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();
    if ($exception instanceof ServiceException) {
      $this->logger->get('php')->error('Quickbooks Online API SDK Service Exception');
      $response = new Response('Quickbooks Online API SDK Service Exception', 500);
      $event->setResponse($response);
    }
    if ($exception instanceof SdkException) {
      $this->logger->get('php')->error('Quickbooks Online API SDK Exception');
      $response = new Response('Quickbooks Online API SDK Exception', 500);
      $event->setResponse($response);
    }
    if ($exception instanceof IdsException) {
      $this->logger->get('php')->error('Quickbooks Online API SDK Ids Exception');
      $response = new Response('Quickbooks Online API SDK Ids Exception', 500);
      $event->setResponse($response);
    }
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException', 60];
    return $events;
  }
}
