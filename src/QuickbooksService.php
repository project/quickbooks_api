<?php

namespace Drupal\quickbooks_api;

/**
 * QuickbooksService is a service to make use of the QBO SDK functions
 * It makes the QBO Dataservice available to use in custom modules
 */

use Drupal\Core\Session\AccountInterface;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\Core\CoreConstants;
use QuickBooksOnline\API\DataService\DataService;

class QuickbooksService {

  private $currentUser;
  private $company_id;
  private $client_id;
  private $client_secret;
  private $access_token;
  private $access_token_expiry;
  private $refresh_token;
  private $refresh_token_expiry;
  private $qbo_environment;
  private $qbo_url;
  private $qbo_oauth_url;

  /**
   * Part of the DependencyInjection magic happening here.
   */
  public function __construct(AccountInterface $currentUser) {

    $config = \Drupal::service('config.factory')->getEditable('quickbooks_api.adminsettings');
    $this->currentUser = $currentUser;

    // Get some config variables
    $this->company_id = $config->get('company_id');
    $this->client_id = $config->get('client_id');
    $this->client_secret = $config->get('client_secret');
    $this->qbo_environment = $config->get('production'); 

    if ($this->qbo_environment) {
       if ($this->qbo_environment < 2) {
          $this->qbo_url = $config->get('intuit_prod_url');
       } else {
          $this->qbo_url = $config->get('intuit_dev_url');
       }
    }
    // Needed for authentication
    $this->qbo_oauth_url = 'quickbooks_api/oauth';

    // State API - get tokens if they are there
    $this->access_token = \Drupal::state()->get('quickbooks_api.access_token');
    $this->access_token_expiry = \Drupal::state()->get('quickbooks_api.access_token_expiry');
    $this->refresh_token = \Drupal::state()->get('quickbooks_api.refresh_token');
    $this->refresh_token_expiry = \Drupal::state()->get('quickbooks_api.refresh_token_expiry');

  }

  /**
   * Validate credentials
   */
  private function validateCredentials() {

    if (!$this->company_id || !$this->client_id || !$this->client_secret || !$this->qbo_environment || !$this->qbo_url) {
       return false;
    } else {
       return true;
    }
  }

  /**
   * Create a QBO API DataService.
   *
   * @return DataService
   *   A QBO API Data Service client.
   */
  public function dataService() {
    global $base_url;
    $config = \Drupal::service('config.factory')->getEditable('quickbooks_api.adminsettings');

    // Return if already initialized.
    if (property_exists($this, 'dataService')) {
       return $this->dataService;
    }

    // Validate all variables. If we don't have them all, we can't do anything
    if (!$this->validateCredentials() || !$this->refresh_token) {
      \Drupal::messenger()->addMessage("Connection to Quickbooks Online is not yet established. Please configure your connection.");
      \Drupal::logger('quickbooks_api')->error("Quickbooks dataService: Connection to Quickbooks Online is not yet established. Please configure your connection.");
      return NULL;
    }

    // Create the service.
    // We can only get here if we have the credentials
    $service = $this->service();

    // Disable logging.
    $service->IppConfiguration->Logger->RequestLog->EnableRequestResponseLogging = FALSE;

    // Create settings array based on environment
    if ($this->qbo_environment == '3') {
       $qbo_base_url = 'Development';
    } else if ($this->qbo_environment == 2 OR $this->qbo_environment == 1) {
       $qbo_base_url = 'Production';
    } else {
       $qbo_base_url = '';
       \Drupal::messenger()->addMessage("Connection to Quickbooks Online is not yet established. Please configure your connection.");
       \Drupal::logger('quickbooks_api')->error("Quickbooks dataService: Connection to Quickbooks Online is not yet established. Please configure your connection.");
       return NULL;
    }

    $settings = array(
                'QBORealmID' => $this->company_id,
                'ClientID' => $this->client_id,
                'ClientSecret' => $this->client_secret,
                'auth_mode' => 'oauth2',
                'baseUrl' => $qbo_base_url,
                'scope' => 'com.intuit.quickbooks.accounting',
                'AccessToken' => $this->access_token,
                'RefreshToken' => $this->refresh_token,
                'redirectURL' => $base_url . '/' . $this->qbo_oauth_url,
    );

    $this->dataService = DataService::Configure($settings);
    // Do we need to go through all the steps??
    // https://developer.intuit.com/docs/00_quickbooks_online/2_build/40_sdks/03_php/0020_authorization
    $OAuth2LoginHelper = $this->dataService->getOAuth2LoginHelper();
    $accessTokenObj = $OAuth2LoginHelper->refreshToken();
    $error = $OAuth2LoginHelper->getLastError();
    if ($error) {
       \Drupal::messenger()->addMessage("The Status code is: " . $error->getHttpStatusCode());
       \Drupal::messenger()->addMessage("The Helper message is: " . $error->getOAuthHelperError());
       \Drupal::messenger()->addMessage("The Response message is: " . $error->getResponseBody());
       \Drupal::logger('quickbooks_api')->error("Quickbooks OAuth2LoginHelper: Status code: ".$error->getHttpStatusCode());
       \Drupal::logger('quickbooks_api')->error("Quickbooks OAuth2LoginHelper: Status code: ".$error->getOAuthHelperError());
       \Drupal::logger('quickbooks_api')->error("Quickbooks OAuth2LoginHelper: Status code: ".$error->getResponseBody());
       return NULL;
    }

    $accessTokenExpiry = $accessTokenObj->getAccessTokenExpiresAt();
    $refreshTokenExpiry = $accessTokenObj->getRefreshTokenExpiresAt();

    // Check if refresh token has been updated
    if ($this->refresh_token == $accessTokenObj->getRefreshToken() && $this->refresh_token_expiry == strtotime($accessTokenObj->getRefreshTokenExpiresAt())) {
       // Token is the same and has not been updated. But it should have been.
       \Drupal::logger('quickbooks_api')->error('Refresh token has not been updated');
    } else {
       // Token has been updated
       \Drupal::logger('quickbooks_api')->notice('Refresh token has been updated');
       $this->refresh_token = $accessTokenObj->getRefreshToken();
       $this->refresh_token_expiry = strtotime($accessTokenObj->getRefreshTokenExpiresAt());
    }

    // State API: save the updated tokens
    \Drupal::state()->set('quickbooks_api.access_token', $accessTokenObj->getAccessToken());
    \Drupal::state()->set('quickbooks_api.refresh_token', $accessTokenObj->getRefreshToken());
    \Drupal::state()->set('quickbooks_api.access_token_expiry', strtotime($accessTokenObj->getAccessTokenExpiresAt()));
    \Drupal::state()->set('quickbooks_api.refresh_token_expiry', strtotime($accessTokenObj->getRefreshTokenExpiresAt()));

    // Update the DataService object with the updated tokens
    $this->dataService->updateOAuth2Token($accessTokenObj);
    return $this->dataService;
  }

  /**
   * Create a QBO API PlatformService class.
   *
   * @return PlatformService
   *   A QBO API Platform Service client class.
   */
  public function platformService() {

    // Return if already initialized.
    if (property_exists($this, 'platformService')) {
      return $this->platformService;
    }

    // Initialize the Platform Service.
    return $this->platformService = new QuickBooksOnline\API\PlatformService\PlatformService($this->service());
  }


  /**
   * Create a service context used to create service classes.
   *
   * @return
   *   A ServiceContext class.
   */
  private function service() {

    global $base_url;

    if (!$this->validateCredentials()) {
      \Drupal::messenger()->addMessage("Quickbooks Service: No valid Quickbooks Online credentials found!");
      \Drupal::logger('quickbooks_api')->error("Quickbooks Service: No valid Quickbooks Online credentials found!");
      return false;
    }

    if ($this->qbo_environment == '3') {
       $qbo_base_url = 'Development';
    } else if ($this->qbo_environment == 2 OR $this->qbo_environment == 1) {
       $qbo_base_url = 'Production';
    } else {
       $qbo_base_url = '';
       \Drupal::messenger()->addMessage("Quickbooks Service: No Quickbooks Online environment selected!");
       \Drupal::logger('quickbooks_api')->error("Quickbooks Service: No valid Quickbooks Online environment selected!");
       return false;
    }

    $settings = array(
                'QBORealmID' => $this->company_id,
                'ClientID' => $this->client_id,
                'ClientSecret' => $this->client_secret,
                'auth_mode' => 'oauth2',
                'baseUrl' => $qbo_base_url,
                'scope' => 'com.intuit.quickbooks.accounting',
                'AccessToken' => $this->access_token,
                'RefreshToken' => $this->refresh_token,
                'redirectURL' => $base_url . '/' . $this->qbo_oauth_url,
    );

    $service_context = \QuickBooksOnline\API\Core\ServiceContext::ConfigureFromPassedArray($settings);

    // Check that the service context was created.
    if (!$service_context) {
       \Drupal::messenger()->addMessage("Quickbooks Service: Could not initialize a service context. Verify your client_id and client_secret are valid!");
       \Drupal::logger('quickbooks_api')->error("Quickbooks Service: Could not initialize a service context. Verify your client_id and client_secret are valid!");
       return NULL;
    }

    return $service_context;

  }

}
