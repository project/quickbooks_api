<?php
/**
 * @file
 * Contains Drupal\quickbooks_api\Form\QuickbooksApiAdminSettingsForm.
 */
namespace Drupal\quickbooks_api\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\Core\CoreConstants;
use QuickBooksOnline\API\DataService\DataService;

class QuickbooksApiAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'quickbooks_api.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quickbooks_api_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $config = $this->config('quickbooks_api.adminsettings');

    $form['requirements'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Requirements'),
    ];

    if (class_exists('\QuickBooksOnline\API\Core\ServiceContext')) {
      $quickbooks_sdk = true;
      $quickbooks_sdk_version = \QuickBooksOnline\API\Core\CoreConstants::DEFAULT_SDK_MINOR_VERSION;
      $quickbooks_sdk_path = new \ReflectionClass('QuickBooksOnline\API\Core\CoreConstants');
      if ($quickbooks_sdk_path) {
         $quickbooks_sdk_filepath = str_replace("/src/Core/CoreConstants.php","",$quickbooks_sdk_path->getFileName());
         $quickbooks_sdk_json = $string = file_get_contents($quickbooks_sdk_filepath.'/composer.json');
         $quickbooks_sdk_array = json_decode($quickbooks_sdk_json, true);
         $quickbooks_sdk_description =  '<p>'.$quickbooks_sdk_array['name'].'<br />';
         $quickbooks_sdk_description .= $quickbooks_sdk_array['description'].'<br />';
         $quickbooks_sdk_description .= 'License: '.$quickbooks_sdk_array['license'].'<br />';
         $quickbooks_sdk_description .= 'Author(s): ';
         foreach ($quickbooks_sdk_array['authors'] AS $key => $author) {
           $quickbooks_sdk_description .= $author['name'] . ' Email: ' .$author['email'].'<br />';
         }
         $quickbooks_sdk_version .= ' ('.str_replace("V3PHPSDK","",\QuickBooksOnline\API\Core\CoreConstants::USERAGENT).')';
      }

      // Get URLs
      $prod_url = \QuickBooksOnline\API\Core\CoreConstants::QBO_BASEURL;
      $dev_url = \QuickBooksOnline\API\Core\CoreConstants::SANDBOX_DEVELOPMENT;
    } else {
      $quickbooks_sdk = false;
      $prod_url = 'https://quickbooks.api.intuit.com';
      $dev_url = 'https://sandbox-quickbooks.api.intuit.com';
    }

    $form['requirements']['quickbooks_sdk'] = [
      '#markup' => ($quickbooks_sdk) ? 'Quickbooks SDK installed. <br />Version: '.$quickbooks_sdk_version.$quickbooks_sdk_description : 'Quickbooks SDK not installed. Please install via <strong>composer require quickbooks/v3-php-sdk</strong>',
    ];

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Connection'),
    ];

    $form['connection']['company_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company ID'),
      '#description' => $this->t('Company ID from Quickbooks online'),
      '#default_value' => $config->get('company_id'),
    ];

    $form['connection']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Client ID for the Quickbooks Online App'),
      '#default_value' => $config->get('client_id'),
      '#required' => true,
    ];

    $form['connection']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#description' => $this->t('Client secret for the Quickbooks Online App'),
      '#default_value' => $config->get('client_secret'),
      '#required' => true,
    ];

    // Here we need more fields to actually connect to Quickbooks
    $form['connection']['oauth'] = [
      '#type' => 'fieldset',
      '#title' => t('OAuth connection'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#access' => (bool) ($config->get('company_id') && $config->get('client_id') && $config->get('client_secret')),
      '#access' => true,
    ];
    // Oauth connection logic here

    // This is our connect button. This needs to use JS!
    $form['connection']['oauth']['button'] = [
      '#access' => (bool) ( \Drupal::state()->get('quickbooks_api.access_token') && (\Drupal::state()->get('quickbooks_api.refresh_token_expiry') < \Drupal::time()->getCurrentTime()) || !\Drupal::state()->get('quickbooks_api.access_token')),
      '#children' => '<ipp:connectToIntuit></ipp:connectToIntuit>',
    ];

    // Show the data, but only if we are connected already
    if ( \Drupal::state()->get('quickbooks_api.access_token') && ( \Drupal::state()->get('quickbooks_api.refresh_token_expiry') > \Drupal::time()->getCurrentTime() ) ) {
      // We get the textual info.
      $qbo=\Drupal::service("quickbooks_api.QuickbooksService");
      $qbo_company = $qbo->dataService()->getCompanyInfo()->CompanyName;
      $qbo_info =  '<p><strong>Connected to Quickbooks Online company:</strong> :company<br />';
      $qbo_info .= '<strong>Refresh token expires on:</strong> :date</p>';
      $form['connection']['oauth']['refresh_expiry'] = [
        '#markup' => $this->t($qbo_info, [':company' => $qbo_company, ':date' => date('d-m-Y', \Drupal::state()->get('quickbooks_api.refresh_token_expiry'))]),
      ];
    }
    // Disconnect and Reconnect button
    //$form['connection']['oauth']['reconnect'] = [
    //  '#type' => 'submit',
    //  '#value' => t('Reconnect'),
    //  '#limit_validation_errors' => array(),
    //  '#submit' => array('qbo_api_config_form_oauth_reconnect'),
    //  '#access' => (bool) $config->get('refresh_token') && qbo_api_token_needs_refresh($this->keys['auth_date']),
    //];

    // Disconnect button: show when we are connected and refresh token has NOT expired
    $form['connection']['oauth']['disconnect'] = [
      '#type' => 'submit',
      '#value' => t('Disconnect'),
      '#limit_validation_errors' => [],
      '#submit' => ['quickbooks_api_config_form_oauth_disconnect'],
      '#access' => (bool) (\Drupal::state()->get('quickbooks_api.access_token') && \Drupal::state()->get('quickbooks_api.refresh_token_expiry') > \Drupal::time()->getCurrentTime()),
    ];
    // End of oauth connection logic

    // Here we have additional fields
    $form['connection']['production'] = [
      '#type' => 'select',
      '#options' => ['1' => 'Production',
                    '2' => 'Production test',
                    '3' => 'Development',],
      '#title' => $this->t('Production or development'),
      '#description' => $this->t('Choose the environment: <strong>Production</strong>: full functionality, <strong>Production test</strong>: uses production environment but does not save data, <strong>Development</strong>: development environment'),
      '#default_value' => $config->get('production'),
      '#required' => true,
    ];

    $form['connection']['intuit_prod_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Intuit production URL'),
      '#description' => $this->t('The Intuit production URL (should be :prod_url)',[':prod_url' => $prod_url]),
      '#default_value' => $config->get('intuit_prod_url'),
      '#required' => true,
    ];

    $form['connection']['intuit_dev_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Intuit development URL'),
      '#description' => $this->t('The Intuit development URL (should be :dev_url)',[':dev_url' => $dev_url]),
      '#default_value' => $config->get('intuit_dev_url'),
      '#required' => true,
    ];

    // Needed for authentication, but only possible if we have no connection.
    $qbo_oauth_url = 'quickbooks_api/oauth';

    if ($config->get('production')) {
      if ($config->get('production') < 2) {
         $qbo_url = $config->get('intuit_prod_url');
         $qboBaseUrl = 'Production';
      } else {
         $qbo_url = $config->get('intuit_dev_url');
         $qboBaseUrl = 'Development';
      }
    }

    if ($qboBaseUrl) {
      // Prepare Data Services
      $dataService = DataService::Configure([
        'auth_mode' => 'oauth2',
        'ClientID' => $config->get('client_id'),
        'ClientSecret' => $config->get('client_secret'),
        'RedirectURI' => $base_url . '/'. $qbo_oauth_url,
        'scope' => "com.intuit.quickbooks.accounting",
        'baseUrl' => $qboBaseUrl,
      ]);

      $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
      $qbo_state = $OAuth2LoginHelper->getState();

      $authorizationUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

      // Attach JS and Drupal Settings to the form
      $form['#attached']['drupalSettings']['quickbooks_api']['quickbooks_api'] = [
        'oauthUrl' => $authorizationUrl,
      ];

      $form['#attached']['library'][] = 'quickbooks_api/quickbooks_api';
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('quickbooks_api.adminsettings')
      ->set('company_id', $form_state->getValue('company_id'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('production', $form_state->getValue('production'))
      ->set('intuit_prod_url', $form_state->getValue('intuit_prod_url'))
      ->set('intuit_dev_url', $form_state->getValue('intuit_dev_url'))
      ->save();
  }

}
