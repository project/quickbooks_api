<?php
namespace Drupal\quickbooks_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use QuickBooksOnline\API\DataService\DataService;

/**
 * Defines QBOCronController class.
 */
class QBOCronController extends ControllerBase {

  /**
   * Function to be run on cron
   * Can be called via hook_cron or via EventSubscriber
   * once there is an equivalent event to hook_cron
   */

  function qboRunCron() {

    // Once a day is enough
    $interval = 3600 * 24;

    // We usually don't want to act every time cron runs (which could be every
    // minute) so keep a time for the next run in the site state.
    $next_execution = \Drupal::state()
      ->get('quickbooks_api.next_execution', 0);

    if (\Drupal::time()->getRequestTime() >= $next_execution) {
      // We need our config
      $config = \Drupal::service('config.factory')->getEditable('quickbooks_api.adminsettings');

      // We check the expiry date of the refresh token
      $refresh_token_expiry = (int)\Drupal::state()->get('quickbooks_api.refresh_token_expiry');
      $refresh_token_renew  = (int)$refresh_token_expiry - (20*24*3600);
      // The refresh token should have a validity of about 100 days. So we refresh it if it is valid for less
      // than 20 days, to keep us on the safe side.

      if (\Drupal::time()->getRequestTime() >= $refresh_token_renew) {
        // We need to initialize the Quickbooks API service
        $qbo_environment = $config->get('production'); 

        if ($qbo_environment) {
          if ($qbo_environment < 2) {
            $qbo_url = $config->get('intuit_prod_url');
            $qboBaseUrl = 'Production';
          } else {
            $qbo_url = $config->get('intuit_dev_url');
            $qboBaseUrl = 'Development';
          }
        }
        // Needed for authentication
        $qbo_oauth_url = 'quickbooks_api/oauth';

        // Prepare Data Services
        $dataService = DataService::Configure([
          'auth_mode' => 'oauth2',
          'ClientID' => $config->get('client_id'),
          'ClientSecret' => $config->get('client_secret'),
          'RedirectURI' => $base_url . '/'. $qbo_oauth_url,
          'scope' => "com.intuit.quickbooks.accounting",
          'baseUrl' => $qboBaseUrl,
        ]);

        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
        $accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken(\Drupal::request()->query->get('code'), \Drupal::request()->query->get('realmId'));

        $accessToken = $accessTokenObj->getAccessToken();
        $refreshToken = $accessTokenObj->getRefreshToken();

        $accessTokenExpiry = $accessTokenObj->getAccessTokenExpiresAt();
        $refreshTokenExpiry = $accessTokenObj->getRefreshTokenExpiresAt();

        // Check if refresh token has been updated
        if (\Drupal::state()->get('quickbooks_api.refresh_token') == $accessTokenObj->getRefreshToken() && \Drupal::state()->get('quickbooks_api.refresh_token_expiry') == strtotime($accessTokenObj->getRefreshTokenExpiresAt())) {
           // Token is the same and has not been updated. But it should have been.
           \Drupal::logger('quickbooks_api')->error('Refresh token has not been updated');
        } else {
           // Token has been updated
           \Drupal::logger('quickbooks_api')->notice('Refresh token has been updated');
           $this->refresh_token = $accessTokenObj->getRefreshToken();
           $this->refresh_token_expiry = strtotime($accessTokenObj->getRefreshTokenExpiresAt());
        }
        // State API: save the tokens and their expiry timestamps
        \Drupal::state()->set('quickbooks_api.access_token', $accessToken);
        \Drupal::state()->set('quickbooks_api.refresh_token', $refreshToken);
        \Drupal::state()->set('quickbooks_api.access_token_expiry', strtotime($accessTokenExpiry));
        \Drupal::state()->set('quickbooks_api.refresh_token_expiry', strtotime($refreshTokenExpiry));

        \Drupal::logger('quickbooks_api')->notice('quickbooks_api cron ran. Refresh token renewed. New refresh token expiry: '.$refreshTokenExpiry);
      } else {
        \Drupal::logger('quickbooks_api')->notice('quickbooks_api cron ran. No need to renew tokens');
      }
      \Drupal::state()
      ->set('quickbooks_api.next_execution', \Drupal::time()->getRequestTime() + $interval);
    }
  }

}
