<?php
namespace Drupal\quickbooks_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use QuickBooksOnline\API\DataService\DataService;

/**
 * Defines QBOOauthController class.
 */
class QBOOauthController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    global $base_url;

    $build = [
      '#markup' => $this->t('Connection with Quickbooks Online'),
    ];

    // Let's see what we have
    if (\Drupal::request()->query->get('realmId') && \Drupal::request()->query->get('code')) {
       // We do have the return from Quickbooks
       $config = \Drupal::service('config.factory')->getEditable('quickbooks_api.adminsettings');

       $qbo_environment = $config->get('production'); 

       if ($qbo_environment) {
         if ($qbo_environment < 2) {
           $qbo_url = $config->get('intuit_prod_url');
           $qboBaseUrl = 'Production';
         } else {
           $qbo_url = $config->get('intuit_dev_url');
           $qboBaseUrl = 'Development';
         }
       }
       // Needed for authentication
       $qbo_oauth_url = 'quickbooks_api/oauth';

       // Prepare Data Services
       $dataService = DataService::Configure([
         'auth_mode' => 'oauth2',
         'ClientID' => $config->get('client_id'),
         'ClientSecret' => $config->get('client_secret'),
         'RedirectURI' => $base_url . '/'. $qbo_oauth_url,
         'scope' => "com.intuit.quickbooks.accounting",
         'baseUrl' => $qboBaseUrl,
       ]);

       $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
       $accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken(\Drupal::request()->query->get('code'), \Drupal::request()->query->get('realmId'));

       $accessToken = $accessTokenObj->getAccessToken();
       $refreshToken = $accessTokenObj->getRefreshToken();

       $accessTokenExpiry = $accessTokenObj->getAccessTokenExpiresAt();
       $refreshTokenExpiry = $accessTokenObj->getRefreshTokenExpiresAt();

       // State API: save the tokens and their expiry timestamps
       \Drupal::state()->set('quickbooks_api.access_token', $accessToken);
       \Drupal::state()->set('quickbooks_api.refresh_token', $refreshToken);
       \Drupal::state()->set('quickbooks_api.access_token_expiry', strtotime($accessTokenExpiry));
       \Drupal::state()->set('quickbooks_api.refresh_token_expiry', strtotime($refreshTokenExpiry));

       // Update our markup array
       $build['#markup'] = $this->t('Connection with Quickbooks Online: established!');
    }
    // Attach our Javascript
    $build['#attached']['library'][] = 'quickbooks_api/quickbooks_api_close';

    return $build;
  }

}
