# Quickbooks API

Quickbooks API is a simplified version for D8 of the D7 module [Quickbooks Online API](https://www.drupal.org/project/qbo_api) . It is a simple integration with the [Quickbooks Online API](https://developer.intuit.com/v2/apiexplorer?apiname=V3QBO#?id=Account) and not intended for Desktop versions of Quickbooks. [Quickbooks Online](http://quickbooks.intuit.com/online/) is a leading business accounting solution. Using this module requires a Quickbooks Online account of your own. Quickbooks Online is a product of [Intuit](https://www.intuit.com/) .

## Dependencies

* Quickbooks Online API SDK

Install the Quickbooks Online API SDK via [composer](https://getcomposer.org/) :

```bash
composer require quickbooks/v3-php-sdk
```

## Installation

We are working on installation via [composer](https://getcomposer.org/) for when this module is full Drupal project. For now, first install the dependencies and then install the module manually.

## Configuration

The Quickbooks API module can be configured at /admin/config/quickbooks_api/adminsettings . The connection to Quickbooks Online is via oauth2. To use the Quickbooks API module, you will need a Intuit developer account and create an app. You can then connect your site to this app via the Quickbooks API module

## Usage

The Quickbooks API module provides a Quickbooks Service. To make use of Quickbooks Online, you need to initialize the service:

```php
$qbo = \Drupal::service("quickbooks_api.QuickbooksService");
```

You can then use the dataService provided by the Quickbooks Online API SDK:

```php
$result = $qbo->dataService()->query(...);
```

You can use all functions provided by the Quickbooks Online API SDK.

## Notes

This module provides no user interface other than the admin configuration form. It is intended for developers.
